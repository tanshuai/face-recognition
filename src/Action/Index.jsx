
const fetchProfile = () => {
    return (dispatch,getState) => {
        console.log("init",getState());
        dispatch({type:"isFetching"});
        fetch("https://api.github.com/users/TANSHUAI1001")
        .then(response => response.json())
        .then((responseData) => {
            console.log(responseData);
            dispatch({type:"fetchStatus",value:"succeed"});
            console.log("succeed",getState());
        }).catch((err) => {
            console.log(err);
            dispatch({type:"fetchStatus",value:"failed"});
            console.log("failed",getState());
        });
    }
}


export { fetchProfile }