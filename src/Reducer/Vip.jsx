
const Vip = (state = {isFetching:false,status:undefined},action) => {
    switch(action.type){
        case "isFetching":
            return {...state,isFetching:true};
        case "fetched":
            return {...state,isFetching:false};
        case "fetchStatus":
            return {...state,status:action.value,isFetching:false};
        default:
            return state;
    }
}

export default Vip