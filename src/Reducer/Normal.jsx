
const Normal = (state={refresh:false,faceset_token:"299888620cd8dec475b083e7f6c3127e"}, action) => {
    switch(action.type){
        case "RESET_REFRESH":
            return {...state,refresh:false};
        case "SET_REFRESH":
            return {...state,refresh:true,loading:true};
        case "SHOW_BUTTON":
            return {...state,showBtn:true,refresh:true,loading:false};
        case "RESET":
            return {...state,showBtn:false,refresh:false};
        case "SET_FACESET_TOKEN":
            return {...state,faceset_token:action.faceset_token};
        case "RESET_LOADING":
            return {...state,loading:false};
        default:
            return state;
    }
}

export default Normal