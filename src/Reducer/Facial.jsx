
const Facial = (state={rectArr:[]},action) => {
    let {rectArr} = state;
    switch(action.type){
        case "ADD_RECT":
            rectArr = [...rectArr,action.value];
            return {...state,rectArr};
        case "ADD_RECTS":
            rectArr = rectArr.concat(action.value);
            return {...state,rectArr};
        case "REST_RECT":
            rectArr = [];
            return {...state,rectArr}; 
        default:
            return state;
    }
}

export default Facial;