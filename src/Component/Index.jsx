import React, { Component } from 'react';
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, BrowserRouter, HashRouter,Link  } from 'react-router-dom';
import Vip from "./Vip";
import Normal from "./Normal";
import Feature from "./Feature";
import Similar from "./Similar";
import Loading from "./Loading";
import Facial from "./Facial";
import { Button, Icon } from 'antd';

export default class Index extends Component{

    render(){
        return(
            <div> welcome, vistor! 
                <div>
                    <Link to="/vip">vip</Link>
                    {"  "}
                    <Link to="/normal">normal</Link>
                    {"  "}
                    <Link to="/feature">feature</Link>
                    {"  "}
                    <Link to="/similar">similar</Link>
                    {"  "}
                    <Link to="/facial">facial</Link>
                </div>
                <Button> ok </Button>
                
            </div>
        )
    }
}

export { Vip, Normal, Feature, Similar, Facial };