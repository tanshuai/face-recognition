import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../Tracking/tracking-min.js';
import ImageSrc1 from "../Tracking/assets/brief1.png";
import ImageSrc2 from "../Tracking/assets/brief2.png";
import '../Style/demo.css';



let demoContainer = {
  background: "#131112"
}
let imageStyle = {
  position: "absolute",
  left: -1000,
  top: -1000
}
let canvasStyle = {
  position: "absolute",
  left: "50%",
  top: "50%",
  margin: "-147px 0 0 -393px"
}


class Similar extends Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        console.log("did mount");
        this.handleLoad();
    }
    handleLoad = ()=>{
        var width = 393;
        var height = 295;
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        var image1 = document.getElementById('image1');
        var image2 = document.getElementById('image2');

        window.descriptorLength = 256;
        window.matchesShown = 30;
        window.blurRadius = 3;

       
    var doMatch = function() {
        tracking.Brief.N = window.descriptorLength;
        context.drawImage(image1, 0, 0, width, height);
        context.drawImage(image2, width, 0, width, height);
    
        var imageData1 = context.getImageData(0, 0, width, height);
        var imageData2 = context.getImageData(width, 0, width, height);
    
        var gray1 = tracking.Image.grayscale(tracking.Image.blur(imageData1.data, width, height, blurRadius), width, height);
        var gray2 = tracking.Image.grayscale(tracking.Image.blur(imageData2.data, width, height, blurRadius), width, height);
    
        var corners1 = tracking.Fast.findCorners(gray1, width, height);
        var corners2 = tracking.Fast.findCorners(gray2, width, height);
    
        var descriptors1 = tracking.Brief.getDescriptors(gray1, width, corners1);
        var descriptors2 = tracking.Brief.getDescriptors(gray2, width, corners2);
    
        var matches = tracking.Brief.reciprocalMatch(corners1, descriptors1, corners2, descriptors2);
        matches.sort(function(a, b) {
            return b.confidence - a.confidence;
        });
    
        for (var i = 0; i < Math.min(window.matchesShown, matches.length); i++) {
            var color = '#' + Math.floor(Math.random()*16777215).toString(16);
            context.fillStyle = color;
            context.strokeStyle = color;
            context.fillRect(matches[i].keypoint1[0], matches[i].keypoint1[1], 4, 4);
            context.fillRect(matches[i].keypoint2[0] + width, matches[i].keypoint2[1], 4, 4);
            context.beginPath();
            context.moveTo(matches[i].keypoint1[0], matches[i].keypoint1[1]);
            context.lineTo(matches[i].keypoint2[0] + width, matches[i].keypoint2[1]);
            context.stroke();
        }
    };
    
    var progress = 0;       //总的加载进度
    image1.onload = function(){
        progress += 50; //加入当前图片的权重
        if(progress===100){
            doMatch();
        }
    }
    image2.onload = function(){
        progress += 50; //加入当前图片的权重
        if(progress===100){
            doMatch(); 
        }
    }

    }

    render(){
        return(
            <div> 
                <h1>similar matches</h1> 
                <div className="demo-frame">
                    <div className="demo-container" style={{...demoContainer}}>
                    <img id="image1" src={ImageSrc1} style={{...imageStyle}}/>
                    <img id="image2" src={ImageSrc2} style={{...imageStyle}}/>
                    <canvas id="canvas" width="786" height="295" style={{...canvasStyle}}></canvas>
                    </div>
                </div>
            </div>
        )
    }
}

export default Similar;