import React, { Component } from 'react';
import './Loading.less';

class Loading extends Component{

    render(){
        let { loading,text,zIndex,style } = this.props;
        !zIndex ? zIndex = 999:false;
        return(
            <div className="loading" style={{display:loading ? "block":"none",zIndex:zIndex,...style}}>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <div> { text } </div>
            </div>
        )
    }
}

export default Loading;