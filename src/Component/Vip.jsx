import React, { Component } from 'react';
import { fetchProfile } from "../Action/Index";
import { connect } from 'react-redux';

class Vip extends Component {
    componentDidMount(){
        let {dispatch} = this.props;
        dispatch(fetchProfile());
    }
    render(){
        return(
            <div> this is vip's view </div>
        );
    }    
}

export default connect(state => state.Vip)(Vip);