import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../Tracking/tracking-min.js';
import ImageSrc from "../Tracking/assets/fast.png";
import '../Style/demo.css';


let demoContainer = {
  background: "#131112"
}
let imageStyle = {
  position: "absolute",
  left: -1000,
  top: -1000
}
let canvasStyle = {
  position: "absolute",
  left: "50%",
  top: "50%",
  margin: "-200px 0 0 -200px"
}


class Feature extends Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        console.log("did mount");
        this.handleLoad();
    }
    handleLoad = ()=>{
        var width = 400;
        var height = 400;
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        var image = document.getElementById('image');
        window.fastThreshold = 10;
        var doFindFeatures = function() {
            tracking.Fast.THRESHOLD = window.fastThreshold;
            console.log("image: ",image)
            context.drawImage(image, 0, 0, width, height);
            var imageData = context.getImageData(0, 0, width, height);
            console.log("image data : ",imageData);
            var gray = tracking.Image.grayscale(imageData.data, width, height);
            console.log("gary: ",gray);
            var corners = tracking.Fast.findCorners(gray, width, height);
            console.log("corners: ",corners);
            for (var i = 0; i < corners.length; i += 2) {
                context.fillStyle = '#f00';
                context.fillRect(corners[i], corners[i + 1], 3, 3);
            }
        };
        image.onload = doFindFeatures; //等待图片加载完成，解决canvas画图空白问题
    }

    render(){
        return(
            <div> 
                <h1>feature detection</h1> 
                <div className="demo-frame">
                    <div className="demo-container" style={{...demoContainer}}>
                    <img id="image" src={ImageSrc} style={{...imageStyle}}/>
                    <canvas id="canvas" width="400" height="400" style={{...canvasStyle}}></canvas>
                    </div>
                </div>
            </div>
        )
    }
}

export default Feature;