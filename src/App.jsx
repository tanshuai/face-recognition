import React, { Component } from 'react';
import ReactDOM, { render } from 'react-dom';
import { Provider } from 'react-redux';
// import route from './Config/Route'; //路由配置
import store from './Config/Store';
import { Router, Route, IndexRoute, BrowserRouter, HashRouter,Switch } from 'react-router-dom';
import Index, { Vip, Normal, Feature, Similar, Facial } from './Component/Index'; 
// import Vip from "./Component/Vip";
// import Normal from "./Component/Normal";
// import Feature from "./Component/Feature";
// import Similar from "./Component/Similar";

import 'whatwg-fetch'; // 兼容浏览器支持fetch
import 'normalize.css'; //重置浏览器默认样式
import 'flex.css'; //flex布局
import './Style/style.less'; //加载公共样式
import './Iconfont/iconfont.css'; //字体图标
import Promise from 'promise-polyfill'; 
// To add to window
if (!window.Promise) {
  window.Promise = Promise;
}

store.subscribe(function () {
    // console.log(store.getState());
});

var HistoryRouter = process.env.NODE_ENV !== 'production' ? BrowserRouter : HashRouter;

render(
    <Provider store={store}>
        <HistoryRouter >
            <Switch>
                <Route exact  path="/" component={Index} />
                <Route exact  path="/vip" component={Vip} />
                <Route exact  path="/normal" component={Normal} />
                <Route exact  path="/feature" component={Feature} />
                <Route exact  path="/similar" component={Similar} />
                <Route exact  path="/facial" component={Facial} />
                
            </Switch>
        </HistoryRouter>
    </Provider>
    ,
    document.body.appendChild(document.createElement('div'))
);