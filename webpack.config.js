var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin'); //css单独打包
var HtmlWebpackPlugin = require('html-webpack-plugin'); //生成html
const sysPath = require('path');
// var publicPath = '/dist/'; //服务器路径
// var path = __dirname + '/dist/';
var publicPath = '/'; //服务器路径
var path = __dirname + '/';

const fs  = require('fs');
const lessToJs = require('less-vars-to-js');
const themeVariables = lessToJs(fs.readFileSync(sysPath.join(__dirname, '/ant-theme-vars.dev.less'), 'utf8'));

const autoprefix = () => ({
  loader: 'postcss-loader',
  options: {
    plugins: () => ([
      require('autoprefixer'),
    ]),
  },
});

var plugins = [];

if (process.argv.indexOf('-p') > -1) { //生产环境
    plugins.push(new webpack.DefinePlugin({ //编译成生产版本
        'process.env': {
            NODE_ENV: JSON.stringify('production')
        }
    }));
    publicPath = '/face-recognition/dist/';
    path = __dirname + '/face-recognition/dist/';
}
plugins.push(new ExtractTextPlugin('[name].css')); //css单独打包

plugins.push(new HtmlWebpackPlugin({ //根据模板插入css/js等生成最终HTML
    path:path,
    filename: 'index.html', //生成的html存放路径，相对于 path
    template: './src/template/index.html', //html模板路径
    hash: true,    //为静态资源生成hash值
}));

module.exports = {
    entry: {
        app: './src/App', //编译的入口文件
    },
    output: {
        publicPath, //编译好的文件，在服务器的路径
        path, //编译到当前目录
        filename: '[name].js' //编译后的文件名字
    },
    devtool: process.argv.indexOf('-p') > -1 ? false : 'source-map' ,
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            }, {
                test: /\.css$/,
                // exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader',  autoprefix()],
                    fallback: 'style-loader',
                })
            }, {
                test: /\.less/,
                // exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader', autoprefix(),
                    {
                        loader:'less-loader',
                        options:{
                            modifyVars: themeVariables
                        }
                    }],
                    fallback: 'style-loader',
                })
            }, {
                test: /\.(eot|woff|svg|ttf|woff2|gif|appcache)(\?|$)/,
                exclude: /node_modules/,
                loader: 'file-loader?name=[name].[ext]'
            }, {
                test: /\.(png|jpg)$/,
                exclude: /node_modules/,
                loader: 'url-loader?limit=20000&name=[name].[ext]' //注意后面那个limit的参数，当你图片大小小于这个限制的时候，会自动启用base64编码图片
            },
            //  {
            //     test: /\.jsx$/,
            //     exclude: /node_modules/,
            //     loaders: ['jsx-loader', 'babel-loader']
            // }
        ]
    },
    devServer: {
        historyApiFallback:true,
        // contentBase: [path.join(__dirname, "static")],
        proxy:{ //代理API
            "/facepp":{
                changeOrigin: true,
                target:"https://api-cn.faceplusplus.com", //后端服务器地址
                pathRewrite:{
                    '^/facepp': '/facepp'
                }
            },
            "/api":{
                target:"http://localhost:8080", //后端服务器地址
                pathRewrite:{
                    '^/api': '/api'
                }
            }, 
        },
        // compress: true,
        port: 3000
    },
    plugins,
    resolve: {
        extensions: ['.js', '.jsx'], //后缀名自动补全
    }
};
