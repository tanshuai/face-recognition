var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');


// 相当于通过本地node服务代理请求到了api-cn.faceplusplus.com/facepp
var proxy = { //代理API
    "/facepp":{
        changeOrigin: true,
        target:"https://api-cn.faceplusplus.com", //后端服务器地址
        pathRewrite:{
            '^/facepp': '/facepp'
        }
    }, 
    "/api":{
        target:"http://localhost:8080", //后端服务器地址
        pathRewrite:{
            '^/api': '/api'
        }
    },
};

//启动服务
var server = new WebpackDevServer(webpack(config), {
    // historyApiFallback:true,
    https:true,
    hot:true,
    inline:true,
    proxy: proxy,
    stats: {
        colors: true
    }
});



server.listen(3000);