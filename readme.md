安装依赖：
```
yarn 
```
或
```
npm install
```
然后运行dev:
```
yarn dev
```
或者
```
npm run dev
```
进入主页然后点击normal标签进入人脸识别调用页面。
注意在Normal.jsx页面下配置自己的key和secret。

配合face-server简易后端服务才能完整运行。